//
//  NewsModel.swift
//  HaberServisi
//
//  Created by Kerim Çağlar on 03/04/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import Foundation

class News{
    
    var haberBaslik:String = String()
    var haberDetay:String = String()
    var haberLink:String = String()
    var haberResim:String = String()
}
