//
//  ViewController.swift
//  HaberServisi
//
//  Created by Kerim Çağlar on 01/04/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit
import AEXML
import SDWebImage

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var titles = [String]()
    var urls = [String]()
    var newsImageUrl = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        guard let
            xmlPath = Bundle.main.path(forResource: "trt-cevre", ofType: "xml"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: xmlPath))
            else { return }
        do{
            let options = AEXMLOptions()
            let xmlDoc = try AEXMLDocument(xml: data, options: options)
            
            let items = xmlDoc.root["item"].all
            
            
            for item in items!
            {
                titles.append(item["title"].string)
                urls.append(item["link"].string)
                
                let enclosure = item["enclosure"]
                
                if let imageUrl = enclosure.attributes["url"]
                {
                    newsImageUrl.append(imageUrl)
                }
            }
            
            print(newsImageUrl)
        }
        catch
        {
            print(error)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //TableView Methodları
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return titles.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! NewsTableViewCell
        
        let imageView = cell.viewWithTag(1) as! UIImageView
        
        imageView.sd_setImage(with: URL(string: newsImageUrl[indexPath.row]))
        
        cell.newsTitle.text = titles[indexPath.row]
        cell.newsUrl.text = urls[indexPath.row]
        
        return cell
    }
    
}

